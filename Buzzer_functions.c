/*
  MIT License

  Copyright (c) 2018 Mina Helmi

  Permission is hereby granted, free of charge, to any person obtaining
  a copy of this software and associated documentation files (the "Software"),
  to deal in the Software without restriction, including without limitation
  the rights to use, copy, modify, merge, publish, distribute, sublicense,
  and/or sell copies of the Software, and to permit persons to whom the
  Software is furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be
  included in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
  ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include "Buzzer_functions.h"
#include <util/delay.h>


inline void Buzzer_vidInit(Buzzer_cptr_t structPtrBuzzerCpy)
{
	DIO_vidSet_pinDirection(structPtrBuzzerCpy->portBuzzer, structPtrBuzzerCpy->pinBuzzer, OUTPUT);
	DIO_vidSet_pinValue(structPtrBuzzerCpy->portBuzzer, structPtrBuzzerCpy->pinBuzzer, structPtrBuzzerCpy->buzzer_off_state);
}

inline void Buzzer_vidActivate(Buzzer_cptr_t structPtrBuzzerCpy)
{
	DIO_vidSet_pinValue(structPtrBuzzerCpy->portBuzzer, structPtrBuzzerCpy->pinBuzzer, !(structPtrBuzzerCpy->buzzer_off_state));
}

inline void Buzzer_vidDeactivate(Buzzer_cptr_t structPtrBuzzerCpy)
{
	DIO_vidSet_pinValue(structPtrBuzzerCpy->portBuzzer, structPtrBuzzerCpy->pinBuzzer, structPtrBuzzerCpy->buzzer_off_state);
}

inline void Buzzer_vidToggle(Buzzer_cptr_t structPtrBuzzerCpy)
{
	DIO_vidToggle_pinValue(structPtrBuzzerCpy->portBuzzer, structPtrBuzzerCpy->pinBuzzer);
}

inline void Buzzer_vidBeep(const u16 u16Timeout_msCpy, Buzzer_cptr_t structPtrBuzzerCpy)
{
	Buzzer_vidActivate(structPtrBuzzerCpy);
	_delay_ms(u16Timeout_msCpy);
	Buzzer_vidDeactivate(structPtrBuzzerCpy);
}

